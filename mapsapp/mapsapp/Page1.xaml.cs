﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace mapsapp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {

        

        public Page1()
        {
            InitializeComponent();
            mapp.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(32.88817, -117.225388), Distance.FromKilometers(5)));


            var pin = new Pin
            {
                Position = new Position(32.888172, -117.225388), //pin1 
                Label = "Qualcomm",      //pin name
                Address = "san diego"     //pin address


            };
            var pin2 = new Pin
            {
                Position = new Position(32.880015, -117.234142), //pin2
                Label = "university of california",              //pin name
                Address = "san diego" //pin address

            };
            var pin3 = new Pin
            {
                Position = new Position(32.864607, -117.252515),    //pin 3
                Label = "la jolla park",            //pin name
                Address = "san diego"               //pin address


            };
            var pin4 = new Pin
            {
                Position = new Position(32.861196, -117.205626),        //pin 4 
                Label = "university city",         //pin name
                Address = "san diego"              // pin address



            };
            var pin5 = new Pin
            {
                Position = new Position(32.885626, -117.223775),   //pin 5
                Label = "hospital",                        //pin name
                Address = "san diego"                      //pin address


            };

            pin.Clicked += Pin_Clicked;
            mapp.Pins.Add(pin);
            mapp.Pins.Add(pin2);
            mapp.Pins.Add(pin3);
            mapp.Pins.Add(pin4);
            mapp.Pins.Add(pin5);


        }
        private void Pin_Clicked(Object sender, EventArgs eventArgs)
        {
            var selectedpin = sender as Pin;
            DisplayAlert(selectedpin?.Label, selectedpin?.Address, "OK");

        }
    }
}